function OptGLCMSizes = generateOptima(nPoints, mse, mse_std, GLCMSizes, GLCMReps, featureNames)

    [~, C1, C2] = getFeatureNames();

    for i = 1:length(featureNames)
        featureName = featureNames{i};
        if any(strcmpi(C1, featureName))
            minima = minimaClass1(featureName, mse, nPoints, GLCMSizes);
        elseif any(strcmpi(C2, featureName))
            minima = minimaClass2(featureName, mse, mse_std, nPoints, GLCMSizes, GLCMReps);
        else
            error('generateOptima:FeatureNameUnknown', ...
                  sprintf(['The provided feature (%s) does not belong ', ...
                           'to any class.'], featureName)); %#ok<SPERR>
        end
        OptGLCMSizes.(featureName) = minima;
    end
end

function minima = minimaClass1(featureName, mse, nPoints, GLCMSizes)

    minima = zeros(1, length(nPoints));
    for j = 1:length(nPoints)
        err = mse{j}.(featureName);

        [minerr, idx] = min(err);
        minima(j) = GLCMSizes(idx);
    end
end

function minima = minimaClass2(featureName, mse, mse_std, nPoints, GLCMSizes, GLCMReps)

    minima = zeros(1, length(nPoints));
    z = 1.96;
    for j = 1:length(nPoints)
        err = mse{j}.(featureName);
        se = sqrt(GLCMReps) * mse_std{j}.(featureName);  % Standardavvikelsen i SE
        bound = err(end) + z * se(end);  % Slutv�rdet plus 2 standardavvikelser
        idx = find(err < bound, 1);  % F�rsta mindre �n gr�nsen
        idx = max(1, min(idx, length(GLCMSizes)));  % Sanity check
        minima(j) = GLCMSizes(idx);
    end
end