classdef Progressor < handle
    
    properties (Access  = private)
        startTime = [];
        lastUpdate = [];
        timeSeries = 0;
        progressSeries = 0;
        description = {'Calculating.'};
        
        padding = 2/3;
        progressBarAndCurveWidth = 9;
        progressBarHeight = 2/3;
        progressCurveHeight = 2;
        progressTextHeight = 1;
        
        progressFigure = [];
        
        progressTextAxis = [];
        progressTextText = [];
        
        progressBarText = [];
        progressBarAxis = [];
        progressBarFrontRectangle = [];
        progressBarBackRectangle = [];
        
        progressCurveAxis = [];
        progressCurveArea = [];
        progressCurveRectangle = [];
        
        progressBackgroundAxis = [];
    end
    
    methods
        
        function obj = Progressor(varargin)
            
            if nargin > 0
                assert(numel(varargin) == 1 && (ischar(varargin{1}) || iscell(varargin{1})), 'Constructor accepts title or zero arguments.');
                if ischar(varargin{1})
                    obj.description = varargin(1);
                else
                    obj.description = varargin{1};
                end
            end
            
            if usejava('jvm')
                
                horizontalSize = [obj.padding obj.progressBarAndCurveWidth obj.padding];
                verticalSize = [obj.padding obj.progressCurveHeight obj.padding obj.progressBarHeight obj.padding obj.progressTextHeight obj.padding];
                axesIndices = [2 2 4 4; 2 2 2 2; 2 2 6 6; 1 3 1 7]; % x1,x2,y1,y2
                [figureWidth, figureHeight, axesPositions] = axesGrid(horizontalSize, verticalSize, axesIndices);
                
                obj.progressFigure = figure('NumberTitle', 'off', 'Resize', 'on', ...
                    'MenuBar', 'none', 'Units', 'centimeters', ...
                    'Position',[0 0 figureWidth figureHeight], ...
                    'HandleVisibility','off','Name','Unknown time remaining.', ...
                    'Color','w','CloseRequestFcn',@Progressor.onCloseFcn);
                
                set(obj.progressFigure, 'Units', 'Normalized');
                position = get(obj.progressFigure, 'OuterPosition');
                position(1:2) = (1 - position(3:4)).*(rand(1,2)*6 + 1)/8;
                set(obj.progressFigure, 'OuterPosition', position);
                
                %set(obj.progressFigure, 'Resize', 'off') % Removed because of a bug which causes mispositioning
                
                if usejava('swing')
                    icon = fullfile(fileparts(mfilename('fullpath')),'cogyellow2.png');
                    if exist(icon, 'file')
                        warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
                        jframe = get(obj.progressFigure,'javaframe');
                        jIcon = javax.swing.ImageIcon(icon);
                        jframe.setFigureIcon(jIcon);
                    end
                end
                
                obj.progressBackgroundAxis = axes('Visible','off','Position',axesPositions(4,:), ...
                    'Parent',obj.progressFigure,'HandleVisibility','off', ...
                    'XLim',[0 1],'YLim',[0 1]);
                obj.progressBarAxis = axes('Visible','off','Position',axesPositions(1,:), ...
                    'Parent',obj.progressFigure,'HandleVisibility','off', ...
                    'XLim',[0 1],'YLim',[0 1]);
                obj.progressCurveAxis = axes('Visible','off','Position',axesPositions(2,:), ...
                    'Parent',obj.progressFigure,'HandleVisibility','off', ...
                    'XLim',[0 1]);
                obj.progressTextAxis = axes('Visible','off','Position',axesPositions(3,:), ...
                    'Parent',obj.progressFigure,'HandleVisibility','off', ...
                    'XLim',[0 1]);
                
                color = rgb2hsv([116 192 69]/255);
                %color(1) = rand;
                color(1) = mod(sum(cat(2,obj.description{:})),256)/256;
                bgcolor = hsv2rgb([color(1) 0.1 1]);
                color = hsv2rgb(color);
                
                obj.progressBarBackRectangle = rectangle('Position',[0 0 1 1],'FaceColor',bgcolor, ...
                    'EdgeColor',color/2,'Parent',obj.progressBarAxis);
                obj.progressBarFrontRectangle = rectangle('Position',[0 0 eps 1],'FaceColor',color, ...
                    'EdgeColor',color/2,'Parent',obj.progressBarAxis);
                obj.progressBarText = text(0.5,0.5,'0 %','VerticalAlignment','middle', ...
                    'HorizontalAlignment','center','Parent',obj.progressBarAxis,'FontName','Calibri','FontSize',10);
                
                obj.progressCurveArea = area(0,0,'FaceColor',color,'EdgeColor',color/2,'Parent',obj.progressCurveAxis);
                obj.progressCurveRectangle = rectangle('Position',[0 0 1 1],'FaceColor',bgcolor, ...
                    'EdgeColor',color/2,'Parent',obj.progressCurveAxis);
                set(obj.progressCurveAxis,'Visible','off','XLim',[0 1]);
                
                obj.progressTextText = text(0,0.5,obj.description,'VerticalAlignment','middle', ...
                    'HorizontalAlignment','left','Parent',obj.progressTextAxis,'FontName','Calibri','FontSize',10,'Interpreter','none');
                
                bgcolor2 = rgb2hsv(bgcolor);
                bgcolor2(1) = rem(bgcolor2(1) + 0.2,1);
                bgcolor2 = hsv2rgb(bgcolor2);
                
                colormap(obj.progressBackgroundAxis,1 - linspace(0,1,64)'*(1-bgcolor2));
                
                xdata = [0 1 1 0];
                ydata = [0 0 1 1];
                cdata = [20 64 20 1];
                patch(xdata,ydata,cdata,'Parent',obj.progressBackgroundAxis,'EdgeColor','none');
            
                drawnow;
                
            else
                fprintf('Created Progressor.\n');
                obj.printDescription();
            end
            
            obj.startTime = now;
            obj.lastUpdate = obj.startTime;
            
        end
        
        function setProgress(obj,progress)
            
            assert(isnumeric(progress) && numel(progress) == 1 && progress <= 1 && progress >= 0, 'Progress must be a numeric scalar between 0 and 1.');
            currentTime = now;
            
            if (currentTime - obj.lastUpdate > 0.1/3600/24) || progress == 1
                
                if progress <= obj.progressSeries(end)
                    obj.timeSeries(obj.progressSeries >= progress) = [];
                    obj.progressSeries(obj.progressSeries >= progress) = [];
                end
                
                obj.timeSeries(end+1) = currentTime - obj.startTime;
                obj.progressSeries(end+1) = progress;
                
                if progress == 0
                    duration = 'Unknown time remaining.';
                    dTdp = 1;
                    dTdpe = 1;
                else
                    dTdp = diff(obj.timeSeries)./diff(obj.progressSeries);
                    dTdp = [dTdp dTdp(end)];
                    if progress == 1
                        duration = 'Completed.';
                        dTdpe = eps;
                    else
                        dTdpe = obj.timeSeries(end)/obj.progressSeries(end);
                        %                         dTdpe = dTdp(end);
                        remainingTime = (1-progress)*dTdpe;
                        duration = Progressor.humanReadableDuration(remainingTime);
                    end
                end
                
                if usejava('jvm')
                    
                    assert(ishandle(obj.progressFigure),'Calculation interrupted by user.');
                    
                    set(obj.progressFigure,'Name', duration);
                    set(obj.progressBarFrontRectangle,'Position',[0,0,max(progress,eps),1]);
                    set(obj.progressBarText,'String',sprintf('%.0f %%',progress*100));
                    
                    [XX,YY] = stairs(obj.progressSeries, dTdp);
                    set(obj.progressCurveArea,'XData',XX,'YData',YY);
                    set(obj.progressCurveRectangle,'Position',[progress 0 max(1-progress,eps) dTdpe])
                    set(obj.progressCurveAxis,'Visible','off','XLim',[0 1],'YLim',[0 1.01*max(max(dTdp),dTdpe)]);
                    obj.lastUpdate = currentTime;
                    
                    drawnow;
                    
                end
            end
            
        end
        
        function setDescription(obj,textString)
            if ischar(textString)
                obj.description = {textString};
            else
                obj.description = textString;
            end
            
            if usejava('jvm')
                set(obj.progressTextText,'String',obj.description);
                drawnow;
            else
                obj.printDescription();
            end
        end
        
        function printDescription(obj)
            fprintf('Progressor: %s\n', obj.description{:});
        end
        
        function printProgress(obj)
            fprintf('Progressor (%.1f %%): %s\n', 100*obj.progressSeries(end), obj.description{:});
        end
        
        function progress = getProgress(obj)
            progress = obj.progressSeries(end);
        end
        
        function delete(obj)
            if usejava('jvm')
                if ishandle(obj.progressFigure)
                    delete(obj.progressFigure)
                end
                drawnow
            else
                fprintf('Deleted Progressor.\n');
                obj.printDescription();
            end
        end
        
    end
    
    methods (Static)
        
        function duration = humanReadableDuration(t)
            if t == 0
                duration = 'Completed.';
            else
                d = floor(t);
                t = (t - d)*24;
                h = floor(t);
                t = (t - h)*60;
                m = floor(t);
                s = (t - m)*60;
                
                duration = '';
                if d ~= 0
                    duration = [duration sprintf('%i d, ',d)];
                end
                if h ~= 0
                    duration = [duration sprintf('%i h, ',h)];
                end
                if m ~= 0
                    duration = [duration sprintf('%i min, ',m)];
                end
                if s ~= 0
                    duration = [duration sprintf('%.1f s',s)];
                end
                duration = [duration ' remaining.'];
            end
        end
        
        function onCloseFcn(~,~)
            selection = questdlg('Would you like to interrupt the calculation in progress?',...
                'Interrupt calculation?',...
                'Yes','No','Yes');
            switch selection,
                case 'Yes',
                    delete(gcbf);
                case 'No'
                    return
            end
        end
        
    end
end

