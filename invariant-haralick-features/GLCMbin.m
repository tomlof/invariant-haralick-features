function Gs = GLCM_bin(pts, ns, reps, nbins, neighbourhood, bounds)

    Gs = cell(reps, length(ns));
    npts = size(pts, 1);
    for i = 1:length(ns)
        n = ns(i);
        for j = 1:reps
            idx = randi(npts, n, 1);
            pt = pts(idx, :);
            G = pt2glcm(pt, nbins, bounds(1), bounds(2), bounds(3), bounds(4));
            Gs{j, i} = G;
        end
    end
