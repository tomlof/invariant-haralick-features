function plotConvergence(GLCMSizes, idealGLCMs, idealFeatures, featureNames)

    colors = [0.0000, 0.4470, 0.7410
              0.8500, 0.3250, 0.0980
              0.9290, 0.6940, 0.1250
              0.4940, 0.1840, 0.5560
              0.4660, 0.6740, 0.1880
              0.3010, 0.7450, 0.9330
              0.6350, 0.0780, 0.1840];

    delta = [0.047, 0.050];
    legendPosX = 0.33;
    legendLeftCorrection = 0.01;

    miny = inf;
    maxy = -inf;
    featuresGrowing1 = {'contrast', 'sumOfSquaresVariance', 'sumAverage', ...
                        'sumVariance', 'differenceVariance', 'autoCorrelation', ...
                        'clusterShade', 'clusterProminence', ...
                        'differenceAverage'};
    featuresGrowing2 = featuresGrowing1;
    featuresZeros1 = {'energy', 'homogeneity', 'maximumProbability', 'inverseDifference'};
    featuresZeros2 = featuresZeros1;
    featuresConverging1 = {'correlation', 'sumEntropy', 'entropy', ...
                           'differenceEntropy', 'informationMeasureOfCorrelation1', ...
                           'informationMeasureOfCorrelation2'};
    featuresConverging2 = featuresConverging1;

    figureHandle = figure();
    figureHandle.Position = [10, 10, 1500, 1500];


    features = featuresGrowing1;
    modsubplot(3, 3, 1, delta);
    for i = 1:length(features)
%         fint = [idealFeatures.(featureNames{i})];
        for j = 1:length(GLCMSizes)
            idealGLCM = idealGLCMs{j};
            [f, err] = GLCMFeatures(idealGLCM, features{i});
            forig(j) = f.(features{i});
        end
        c = colors(mod(i, size(colors, 1)) + 1, :);
        marker = '-';
        if strcmp(features{i}, 'clusterProminence')
%             factor = 56.5;
%             fint = fint * factor;
%             text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
            marker = '--';
        end
        if strcmp(features{i}, 'differenceAverage')
%             factor = 10;
%             fint = fint * factor;
%             text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
            marker = '--';
        end

        semilogy(GLCMSizes, forig, marker, 'color', c, 'LineWidth', 2);
        hold('on');
        drawnow();
    end
%     h = legend(featuresGrowing1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
    xlim([0, 256]);
    ylim([0.1, 1e8]);
    title('Original Features', 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    ylabel('Feature value', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));

    features = featuresConverging1;
    modsubplot(3, 3, 4, delta);
    for i = 1:length(features)
%         fint = [idealFeatures.(featureNames{i})];
        for j = 1:length(GLCMSizes)
            idealGLCM = idealGLCMs{j};
            [f, err] = GLCMFeatures(idealGLCM, features{i});
            forig(j) = f.(features{i});
        end
        c = colors(mod(i, size(colors, 1)) + 1, :);
        plot(GLCMSizes, forig, '-', 'color', c, 'LineWidth', 2);
        hold('on');
        drawnow();
    end
%     h = legend(featuresConverging1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
    xlim([0, 256]);
    ylim([-0.6, 9.1]);
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    ylabel('Feature value', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    set(gca, 'TickLabelInterpreter', 'LaTeX');

    features = featuresZeros1;
    modsubplot(3, 3, 7, delta);
    for i = 1:length(features)
%         fint = [idealFeatures.(featureNames{i})];
        for j = 1:length(GLCMSizes)
            idealGLCM = idealGLCMs{j};
            [f, err] = GLCMFeatures(idealGLCM, features{i});
            forig(j) = f.(features{i});
        end
        c = colors(mod(i, size(colors, 1)) + 1, :);
        if strcmp(features{i}, 'maximumProbability')
            factor = 5;
            forig = forig * factor;
            text(31, forig(30)+0.04, sprintf('$\\times %d$', factor), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'energy')
            factor = 5;
            forig = forig * factor;
        end
        plot(GLCMSizes, forig, '-', 'color', c, 'LineWidth', 2);
        if strcmp(features{i}, 'energy')
            text(2, forig(15)-0.11, sprintf('$\\times %d$', factor), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        hold('on');
        drawnow();
    end
%     h = legend(featuresZeros1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
    xlim([0, 256]);
    ylim([0, 1]);
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    ylabel('Feature value', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    set(gca, 'TickLabelInterpreter', 'LaTeX');





    features = featuresGrowing2;
    modsubplot(3, 3, 2, delta);
    for i = 1:length(features)
        fint = [idealFeatures.(features{i})];
        c = colors(mod(i, size(colors, 1)) + 1, :);
        marker = '-';
        if strcmp(features{i}, 'sumOfSquaresVariance')
            factor = 60;
            fint = fint * factor;
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', factor), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'contrast')
            factor = 345;
            fint = fint * factor;
        end
        if strcmp(features{i}, 'clusterProminence')
            factor = 56.5;
            fint = fint * factor;
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
            marker = '--';
        end
        if strcmp(features{i}, 'clusterShade')
            factor = 306;
            fint = fint * factor;
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'differenceVariance')
            factor = 369;
            fint = fint * factor;
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'autoCorrelation')
            factor = 0.6;
            fint = fint * factor;
%             text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'differenceAverage')
            factor = 10;
            fint = fint * factor;
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
            marker = '--';
        end
        plot(GLCMSizes, fint, marker, 'color', c, 'LineWidth', 2);
        if strcmp(features{i}, 'contrast')
            text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        hold('on');
        drawnow();
    end
    h = legend(featureNameMap(features), 'Interpreter', 'LaTeX', 'Location', 'EastOutside', 'FontSize', fontSize('text', figureHandle));
    h.Position(1) = 1 - legendPosX;
    xlim([0, 256]);
    ylim([0, 1.8]);
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    title('Invariant Features', 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    set(gca, 'TickLabelInterpreter', 'LaTeX');

    features = featuresConverging2;
    modsubplot(3, 3, 5, delta);
    for i = 1:length(features)
        fint = [idealFeatures.(features{i})];
        c = colors(mod(i, size(colors, 1)) + 1, :);
        if strcmp(features{i}, 'differenceEntropy')
            factor = 0.8;
            fint = fint * factor;
%             text(GLCMSizes(end)-40, fint(end)-0.09, sprintf('$\\times %d$', round(factor, -1)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        plot(GLCMSizes, fint, '-', 'color', c, 'LineWidth', 2);
        hold('on');
        drawnow();
    end
    h = legend(featureNameMap(features), 'Interpreter', 'LaTeX', 'Location', 'EastOutside', 'FontSize', fontSize('text', figureHandle));
    h.Position(1) = 1 - legendPosX;
    xlim([0, 256]);
    ylim([-2.3, 2.4]);
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    set(gca, 'TickLabelInterpreter', 'LaTeX');

    features = featuresZeros2;
    modsubplot(3, 3, 8, delta);
    for i = 1:length(features)
        fint = [idealFeatures.(features{i})];
        c = colors(mod(i, size(colors, 1)) + 1, :);
        if strcmp(features{i}, 'homogeneity')
            factor = 7;
            fint = fint * factor;
            text(GLCMSizes(end)-32, fint(end)-1.2, sprintf('$\\times %d$', factor), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        if strcmp(features{i}, 'inverseDifference')
            factor = 4;
            fint = fint * factor;
            text(GLCMSizes(end)-32, fint(end)-1.2, sprintf('$\\times %d$', factor), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
        plot(GLCMSizes, fint, '-', 'color', c, 'LineWidth', 2);
        hold('on');
        drawnow();
    end
    h = legend(featureNameMap(features), 'Interpreter', 'LaTeX', 'Location', 'EastOutside', 'FontSize', fontSize('text', figureHandle));
    h.Position(1) = 1 - legendPosX;
    xlim([0, 256]);
%     ylim([0, 9]);
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    set(gca, 'TickLabelInterpreter', 'LaTeX');










    %%%%%%%%%%%%%%%%%%%%
    % Convergence plot %
    %%%%%%%%%%%%%%%%%%%%
%     figure();
%     miny = inf;
%     maxy = -inf;
%     for i = 1:length(featureNames)
%         fint = [idealFeatures.(featureNames{i})];
%         for j = 1:length(GLCMSizes)
%             idealGLCM = idealGLCMs{j};
%             [f, err] = GLCMFeatures(idealGLCM, featureNames{i});
%             forig(j) = f.(featureNames{i});
%         end
%         subplot(1, 2, 1);
%         fint = fint - fint(end);
%         fint = fint / max(abs(fint));
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         plot(GLCMSizes, fint, '-', 'color', c);
%         hold('on');
%         miny = min(miny, min(fint));
%         maxy = max(maxy, max(fint));
%         axis([min(GLCMSizes), max(GLCMSizes), -1, 1]);  % max(abs(miny), maxy), max(abs(miny), maxy)]);
% 
%         subplot(1, 2, 2);
%         forig = forig - forig(end);
%         forig = forig / max(abs(forig));
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         plot(GLCMSizes, forig, '-', 'color', c);
%         hold('on');
%         axis([min(GLCMSizes), max(GLCMSizes), -1, 1]);
%         drawnow();
%     end
%     subplot(1, 2, 1);
%     title('Invariant Haralick texture features', 'Interpreter', 'LaTeX', 'FontSize', 16);
%     ylabel('Normalised texture features, $f$', 'Interpreter', 'LaTeX');
%     xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX');
% %         legend(featureNames, 'Interpreter', 'LaTeX');
% 
%     subplot(1, 2, 2);
%     title('Original Haralick texture features', 'Interpreter', 'LaTeX', 'FontSize', 16);
%     ylabel('Normalised texture features, $f$', 'Interpreter', 'LaTeX');
%     xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX');
% %         legend(featureNames, 'Interpreter', 'LaTeX');

%     miny = inf;
%     maxy = -inf;
%     featuresGrowing1 = {'contrast', 'sumOfSquaresVariance', 'sumAverage', ...
%                         'sumVariance', 'differenceVariance', 'autoCorrelation', ...
%                         'clusterShade', 'clusterProminence', ...
%                         'differenceAverage'};
%     featuresGrowing2 = featuresGrowing1;
%     featuresZeros1 = {'energy', 'homogeneity', 'maximumProbability', 'inverseDifference'};
%     featuresZeros2 = featuresZeros1;
%     featuresConverging1 = {'correlation', 'sumEntropy', 'entropy', ...
%                            'differenceEntropy', 'informationMeasureOfCorrelation1', ...
%                            'informationMeasureOfCorrelation2'};
%     featuresConverging2 = featuresConverging1;
% 
%     h = figure();
%     h.Position = [10, 10, 1500, 1500];
% 
%     modsubplot(3, 4, 2, delta);
%     for i = 1:length(featuresGrowing1)
% %         fint = [idealFeatures.(featureNames{i})];
%         for j = 1:length(GLCMSizes)
%             idealGLCM = idealGLCMs{j};
%             [f, err] = GLCMFeatures(idealGLCM, featuresGrowing1{i});
%             forig(j) = f.(featuresGrowing1{i});
%         end
% 
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         semilogy(GLCMSizes, forig, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresGrowing1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
%     xlim([0, 256]);
%     title('Original Features', 'Interpreter', 'LaTeX', 'FontSize', titleFontsize);
%     ylabel('Feature value', 'Interpreter', 'LaTeX');
% 
%     modsubplot(3, 4, 6, delta);
%     for i = 1:length(featuresConverging1)
% %         fint = [idealFeatures.(featureNames{i})];
%         for j = 1:length(GLCMSizes)
%             idealGLCM = idealGLCMs{j};
%             [f, err] = GLCMFeatures(idealGLCM, featuresConverging1{i});
%             forig(j) = f.(featuresConverging1{i});
%         end
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         if strcmp(featuresConverging1{i}, 'informationMeasureOfCorrelation1')
%             forig = forig * 25;
%             featuresConverging1{i} = sprintf('%s $\\times$ 25', 'informationMeasureOfCorrelation1');
%         end
%         if strcmp(featuresConverging1{i}, 'informationMeasureOfCorrelation2')
%             forig = forig * 10;
%             featuresConverging1{i} = sprintf('%s $\\times$ 10', 'informationMeasureOfCorrelation2');
%         end
%         if strcmp(featuresConverging1{i}, 'correlation')
%             forig = forig * 8;
%             featuresConverging1{i} = sprintf('%s $\\times$ 8', 'correlation');
%         end
%         plot(GLCMSizes, forig, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresConverging1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
%     xlim([0, 256]);
%     ylabel('Feature value', 'Interpreter', 'LaTeX');
% 
%     modsubplot(3, 4, 10, delta);
%     for i = 1:length(featuresZeros1)
% %         fint = [idealFeatures.(featureNames{i})];
%         for j = 1:length(GLCMSizes)
%             idealGLCM = idealGLCMs{j};
%             [f, err] = GLCMFeatures(idealGLCM, featuresZeros1{i});
%             forig(j) = f.(featuresZeros1{i});
%         end
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         plot(GLCMSizes, forig, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresZeros1, 'Interpreter', 'LaTeX', 'Location', 'WestOutside');
%     h.Position(1) = legendPosX - h.Position(3) - legendLeftCorrection;
%     xlim([0, 256]);
%     ylabel('Feature value', 'Interpreter', 'LaTeX');
% 
% 
% 
%     modsubplot(3, 4, 3, delta);
%     for i = 1:length(featuresGrowing2)
%         fint = [idealFeatures.(featureNames{i})];
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         if strcmp(featuresGrowing2{i}, 'sumVariance')
%             fint = fint * 100;
%             featuresGrowing2{i} = sprintf('%s $\\times$ 100', 'sumVariance');
%         end
%         if strcmp(featuresGrowing2{i}, 'sumOfSquaresVariance')
%             fint = fint * 300;
%             featuresGrowing2{i} = sprintf('%s $\\times$ 300', 'sumOfSquaresVariance');
%         end
%         if strcmp(featuresGrowing2{i}, 'differenceVariance')
%             fint = fint * 5;
%             featuresGrowing2{i} = sprintf('%s $\\times$ 5', 'differenceVariance');
%         end
%         plot(GLCMSizes, fint, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresGrowing2, 'Interpreter', 'LaTeX', 'Location', 'EastOutside');
%     h.Position(1) = 1 - legendPosX;
%     xlim([0, 256]);
%     title('Invariant Features', 'Interpreter', 'LaTeX', 'FontSize', titleFontsize);
% 
%     modsubplot(3, 4, 7, delta);
%     for i = 1:length(featuresConverging2)
%         fint = [idealFeatures.(featureNames{i})];
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         if strcmp(featuresConverging2{i}, 'differenceEntropy')
%             fint = fint * 400;
%             featuresConverging2{i} = sprintf('%s $\\times$ 400', 'differenceEntropy');
%         end
%         if strcmp(featuresConverging2{i}, 'sumEntropy')
%             fint = fint * 300;
%             featuresConverging2{i} = sprintf('%s $\\times$ 300', 'sumEntropy');
%         end
%         if strcmp(featuresConverging2{i}, 'informationMeasureOfCorrelation1')
%             fint = fint * 5;
%             featuresConverging2{i} = sprintf('%s $\\times$ 5', 'informationMeasureOfCorrelation1');
%         end
%         plot(GLCMSizes, fint, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresConverging2, 'Interpreter', 'LaTeX', 'Location', 'EastOutside');
%     h.Position(1) = 1 - legendPosX;
%     xlim([0, 256]);
% 
%     modsubplot(3, 4, 11, delta);
%     for i = 1:length(featuresZeros2)
%         fint = [idealFeatures.(featureNames{i})];
%         c = colors(mod(i, size(colors, 1)) + 1, :);
%         if strcmp(featuresZeros2{i}, 'homogeneity')
%             fint = fint * 400;
%             featuresZeros2{i} = sprintf('%s $\\times$ 400', 'homogeneity');
%         end
%         if strcmp(featuresZeros2{i}, 'inverseDifference')
%             fint = fint * 100;
%             featuresZeros2{i} = sprintf('%s $\\times$ 100', 'inverseDifference');
%         end
%         plot(GLCMSizes, fint, '-', 'color', c, 'LineWidth', 2);
%         hold('on');
%         drawnow();
%     end
%     h = legend(featuresZeros2, 'Interpreter', 'LaTeX', 'Location', 'EastOutside');
%     h.Position(1) = 1 - legendPosX;
%     xlim([0, 256]);
