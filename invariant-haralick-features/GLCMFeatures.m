function [out, err] = GLCMFeatures(glcm, varargin)
% 
% GLCM_Features1 helps to calculate the features from the different GLCMs
% that are input to the function. The GLCMs are stored in a i x j x n
% matrix, where n is the number of GLCMs calculated usually due to the
% different orientation and displacements used in the algorithm. Usually
% the values i and j are equal to 'NumLevels' parameter of the GLCM
% computing function graycomatrix(). Note that matlab quantization values
% belong to the set {1,..., NumLevels} and not from {0,...,(NumLevels-1)}
% as provided in some references
% http://www.mathworks.com/access/helpdesk/help/toolbox/images/graycomatrix
% .html
% 
% Although there is a function graycoprops() in Matlab Image Processing
% Toolbox that computes four parameters Contrast, Correlation, Energy,
% and Homogeneity. The paper by Haralick suggests a few more parameters
% that are also computed here. The code is not fully vectorized and hence
% is not an efficient implementation but it is easy to add new features
% based on the GLCM using this code. Takes care of 3 dimensional glcms
% (multiple glcms in a single 3D array)
% 
% If you find that the values obtained are different from what you expect 
% or if you think there is a different formula that needs to be used 
% from the ones used in this code please let me know. 
% A few questions which I have are listed in the link 
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/239608
%
% I plan to submit a vectorized version of the code later and provide 
% updates based on replies to the above link and this initial code. 
%
% Features computed 
% Autocorrelation: [2]                      (out.autoc)
% Contrast: matlab/[1,2]                    (out.contr)
% Correlation: matlab                       (out.corrm)
% Correlation: [1,2]                        (out.corrp)
% Cluster Prominence: [2]                   (out.cprom)
% Cluster Shade: [2]                        (out.cshad)
% Dissimilarity: [2]                        (out.dissi)
% Energy: matlab / [1,2]                    (out.energ)
% Entropy: [2]                              (out.entro)
% Homogeneity: matlab                       (out.homom)
% Homogeneity: [2]                          (out.homop)
% Maximum probability: [2]                  (out.maxpr)
% Sum of sqaures: Variance [1]              (out.sosvh)
% Sum average [1]                           (out.savgh)
% Sum variance [1]                          (out.svarh)
% Sum entropy [1]                           (out.senth)
% Difference variance [1]                   (out.dvarh)
% Difference entropy [1]                    (out.denth)
% Information measure of correlation1 [1]   (out.inf1h)
% Informaiton measure of correlation2 [1]   (out.inf2h)
% Inverse difference (INV) is homom [3]     (out.homom)
% Inverse difference normalized (INN) [3]   (out.indnc) 
% Inverse difference moment normalized [3]  (out.idmnc)
%
% The maximal correlation coefficient was not calculated due to
% computational instability 
% http://murphylab.web.cmu.edu/publications/boland/boland_node26.html
%
% Formulae from MATLAB site (some look different from
% the paper by Haralick but are equivalent and give same results)
% Example formulae: 
% Contrast = sum_i(sum_j(  (i-j)^2 * p(i,j) ) ) (same in matlab/paper)
% Correlation = sum_i( sum_j( (i - u_i)(j - u_j)p(i,j)/(s_i.s_j) ) ) (m)
% Correlation = sum_i( sum_j( ((ij)p(i,j) - u_x.u_y) / (s_x.s_y) ) ) (p[2])
% Energy = sum_i( sum_j( p(i,j)^2 ) )           (same in matlab/paper)
% Homogeneity = sum_i( sum_j( p(i,j) / (1 + |i-j|) ) ) (as in matlab)
% Homogeneity = sum_i( sum_j( p(i,j) / (1 + (i-j)^2) ) ) (as in paper)
% 
% Where:
% u_i = u_x = sum_i( sum_j( i.p(i,j) ) ) (in paper [2])
% u_j = u_y = sum_i( sum_j( j.p(i,j) ) ) (in paper [2])
% s_i = s_x = sum_i( sum_j( (i - u_x)^2.p(i,j) ) ) (in paper [2])
% s_j = s_y = sum_i( sum_j( (j - u_y)^2.p(i,j) ) ) (in paper [2])
%
% 
% Normalize the glcm:
% Compute the sum of all the values in each glcm in the array and divide 
% each element by it sum
%
% Haralick uses 'Symmetric' = true in computing the glcm
% There is no Symmetric flag in the Matlab version I use hence
% I add the diagonally opposite pairs to obtain the Haralick glcm
% Here it is assumed that the diagonally opposite orientations are paired
% one after the other in the matrix
% If the above assumption is true with respect to the input glcm then
% setting the flag 'pairs' to 1 will compute the final glcms that would result 
% by setting 'Symmetric' to true. If your glcm is computed using the
% Matlab version with 'Symmetric' flag you can set the flag 'pairs' to 0
%
% References:
% 1. R. M. Haralick, K. Shanmugam, and I. Dinstein, Textural Features of
% Image Classification, IEEE Transactions on Systems, Man and Cybernetics,
% vol. SMC-3, no. 6, Nov. 1973
% 2. L. Soh and C. Tsatsoulis, Texture Analysis of SAR Sea Ice Imagery
% Using Gray Level Co-Occurrence Matrices, IEEE Transactions on Geoscience
% and Remote Sensing, vol. 37, no. 2, March 1999.
% 3. D A. Clausi, An analysis of co-occurrence texture statistics as a
% function of grey level quantization, Can. J. Remote Sensing, vol. 28, no.
% 1, pp. 45-62, 2002
% 4. http://murphylab.web.cmu.edu/publications/boland/boland_node26.html
%
%
% Example:
%
% Usage is similar to graycoprops() but needs extra parameter 'pairs' apart
% from the GLCM as input
% I = imread('circuit.tif');
% GLCM2 = graycomatrix(I,'Offset',[2 0;0 2]);
% stats = GLCM_features1(GLCM2,0)
% The output is a structure containing all the parameters for the different
% GLCMs
%
% [Avinash Uppuluri: avinash_uv@yahoo.com: Last modified: 11/20/08]
%
% Vectorized by 
% Patrik Brynolfsson: patrik.brynolfsson@umu.se: Last modified: 2015-11-09


% Features computed 
% Autocorrelation: [2]   
% Cluster Prominence: [2]                   
% Cluster Shade: [2] 
% Contrast: [1]                                         
% Correlation: [1]                        
% Difference entropy [1] 
% Difference variance [1]                   
% Dissimilarity: [2]                        
% Energy: [1]                    
% Entropy: [2]       
% Homogeneity: (Inverse Difference Moment) [2,1] 
% Information measure of correlation1 [1]   
% Informaiton measure of correlation2 [1]  
% Inverse difference (Homogeneity in matlab): [3]                              
% Maximum probability: [2]                    
% Sum average [1]   
% Sum entropy [1]  
% Sum of sqaures: Variance [1]    
% Sum variance [1]   
%
% References:
% 1. R. M. Haralick, K. Shanmugam, and I. Dinstein, Textural Features of
% Image Classification, IEEE Transactions on Systems, Man and Cybernetics,
% vol. SMC-3, no. 6, Nov. 1973
% 2. L. Soh and C. Tsatsoulis, Texture Analysis of SAR Sea Ice Imagery
% Using Gray Level Co-Occurrence Matrices, IEEE Transactions on Geoscience
% and Remote Sensing, vol. 37, no. 2, March 1999.
% 3. D A. Clausi, An analysis of co-occurrence texture statistics as a
% function of grey level quantization, Can. J. Remote Sensing, vol. 28, no.
% 1, pp. 45-62, 2002
%
%
% Started from Avinash Uppupuri's code on Matlab file exchange. It has then
% been vectorized. Three features were not implemented correctly in that
% code, it has since then been changed. The features are: 
%   * Sum of squares: variance
%   * Difference variance
%   * Sum Variance

    if nargin == 0
        error('Not enough input arguments')
    else
        if ((size(glcm, 1) <= 1) || (size(glcm, 2) <= 1))
            error('The GLCM should be a 2-D or 3-D matrix.');
        elseif (size(glcm, 1) ~= size(glcm, 2))
            error('Each GLCM should be square with NumLevels rows and NumLevels cols');
        end
    end

    if nargin < 2
        featureNames = 'all';
    elseif length(varargin) == 1
        featureNames = varargin{1};
    else
        featureNames = cell(1, length(varargin));
        for i = 1:length(varargin)
            featureNames{i} = varargin{i};
        end
    end

    % Get size of GLCM
    nGrayLevels = size(glcm,1);
    nglcm = size(glcm,3);

    % Normalize the GLCMs
    glcm = bsxfun(@rdivide,glcm,sum(sum(glcm)));

    % Checked
    if scmp('energy', featureNames)  % Energy: matlab/[1,2]
        out.energy = zeros(1, nglcm);
        err.energy = zeros(1, nglcm);
    end
    if scmp('contrast', featureNames)  % Contrast: matlab/[1,2]
        out.contrast = zeros(1, nglcm);
        err.contrast = zeros(1, nglcm);
    end
    if scmp('correlation', featureNames)  % Correlation: [1,2]
        out.correlation = zeros(1, nglcm);
        err.correlation = zeros(1, nglcm);
    end
    if scmp('sumOfSquaresVariance', featureNames)  % Sum of sqaures: Variance [1]
        out.sumOfSquaresVariance = zeros(1, nglcm);
        err.sumOfSquaresVariance = zeros(1, nglcm);
    end
    if scmp('homogeneity', featureNames)  % Homogeneity: [2] (inverse difference moment)
        out.homogeneity = zeros(1, nglcm);
        err.homogeneity = zeros(1, nglcm);
    end
    if scmp('sumAverage', featureNames)  % Sum average [1]
        out.sumAverage = zeros(1, nglcm);
        err.sumAverage = zeros(1, nglcm);
    end
    if scmp('sumVariance', featureNames)  % Sum variance [1]
        out.sumVariance = zeros(1, nglcm);
        err.sumVariance = zeros(1, nglcm);
    end
    if scmp('sumEntropy', featureNames)  % Sum entropy [1]
        out.sumEntropy = zeros(1, nglcm);
        err.sumEntropy = zeros(1, nglcm);
    end
    if scmp('entropy', featureNames)  % Entropy: [2]
        out.entropy = zeros(1, nglcm);
        err.entropy = zeros(1, nglcm);
    end
    if scmp('differenceVariance', featureNames)  % Difference variance [1]
        out.differenceVariance = zeros(1, nglcm);
        err.differenceVariance = zeros(1, nglcm);
    end
    if scmp('differenceEntropy', featureNames)  % Difference entropy [1]
        out.differenceEntropy = zeros(1, nglcm);
        err.differenceEntropy = zeros(1, nglcm);
    end
    if scmp('informationMeasureOfCorrelation1', featureNames)  % Information measure of correlation1 [1]
        out.informationMeasureOfCorrelation1 = zeros(1, nglcm);
        err.informationMeasureOfCorrelation1 = zeros(1, nglcm);
    end
    if scmp('informationMeasureOfCorrelation2', featureNames)  % Informaiton measure of correlation2 [1]
        out.informationMeasureOfCorrelation2 = zeros(1, nglcm);
        err.informationMeasureOfCorrelation2 = zeros(1, nglcm);
    end
    if scmp('maximalCorrelationCoefficient', featureNames)  % Maximal Correlation Coefficient [1]
        out.maximalCorrelationCoefficient = zeros(1, nglcm);
        err.maximalCorrelationCoefficient = zeros(1, nglcm);
    end

    if scmp('autoCorrelation', featureNames)  % Autocorrelation: [2]
        out.autoCorrelation = zeros(1, nglcm);
        err.autoCorrelation = zeros(1, nglcm);
    end
    if scmp('dissimilarity', featureNames)  % Dissimilarity: [2]
        out.dissimilarity = zeros(1, nglcm);
        err.dissimilarity = zeros(1, nglcm);
    end
    if scmp('clusterShade', featureNames)  % Cluster Shade: [2]
        out.clusterShade = zeros(1, nglcm);
        err.clusterShade = zeros(1, nglcm);
    end
    if scmp('clusterProminence', featureNames)  % Cluster Prominence: [2]
        out.clusterProminence = zeros(1, nglcm);
        err.clusterProminence = zeros(1, nglcm);
    end
    if scmp('maximumProbability', featureNames)  % Maximum probability: [2]
        out.maximumProbability = zeros(1, nglcm);
        err.maximumProbability = zeros(1, nglcm);
    end
    if scmp('inverseDifference', featureNames)  % Homogeneity in matlab
        out.inverseDifference = zeros(1, nglcm);
        err.inverseDifference = zeros(1, nglcm);
    end
    if scmp('differenceAverage', featureNames)
        out.differenceAverage = zeros(1, nglcm);
        err.differenceAverage = zeros(1, nglcm);
    end

    glcmMean = zeros(nglcm, 1);
    uX = zeros(nglcm, 1);
    uY = zeros(nglcm, 1);
    sX = zeros(nglcm, 1);
    sY = zeros(nglcm, 1);

    % pX pY pXplusY pXminusY
    if scmp('informationMeasureOfCorrelation1', featureNames) ...
    || scmp('informationMeasureOfCorrelation2', featureNames) ...
    || scmp('maximalCorrelationCoefficient', featureNames)
        pX = zeros(nGrayLevels, nglcm);  % Ng x #glcms[1]
        pY = zeros(nGrayLevels, nglcm);  % Ng x #glcms[1]
    end
    if scmp('sumAverage', featureNames) ...
    || scmp('sumVariance', featureNames) ...
    || scmp('sumEntropy', featureNames) ...
    || scmp('sumVariance', featureNames)
        pXplusY = zeros((nGrayLevels * 2 - 1), nglcm);  % [1]
    end
    if scmp('differenceEntropy', featureNames) ...
    || scmp('differenceVariance', featureNames)
        pXminusY = zeros(nGrayLevels, nglcm);  % [1]
    end
    % HXY1 HXY2 HX HY
    if scmp('informationMeasureOfCorrelation1', featureNames)
        HXY1 = zeros(nglcm, 1);
        HX = zeros(nglcm, 1);
        HY = zeros(nglcm, 1);
    end
    if scmp('informationMeasureOfCorrelation2', featureNames)
        HXY2 = zeros(nglcm, 1);
    end

    % Create indices for vectorising code:
    sub = 1:nGrayLevels * nGrayLevels;
    [I, J] = ind2sub([nGrayLevels, nGrayLevels], sub);

    % Loop over all GLCMs
    for k = 1:nglcm
        currentGLCM = glcm(:, :, k);
        glcmMean(k) = mean2(currentGLCM);

        % For symmetric GLCMs, uX = uY
        uX(k) = sum(I .* currentGLCM(sub));
        uY(k) = sum(J .* currentGLCM(sub));
        sX(k) = sum((I - uX(k)).^2 .* currentGLCM(sub));
        sY(k) = sum((J - uY(k)).^2 .* currentGLCM(sub));

        if scmp('sumAverage', featureNames) ...
        || scmp('sumVariance', featureNames) ...
        || scmp('sumEntropy', featureNames)
            tmp1 = [(I + J)' currentGLCM(sub)'];
            idx1 = 2:2 * nGrayLevels;
            for i = idx1
                pXplusY(i - 1, k) = sum(tmp1(tmp1(:, 1) == i, 2));
            end
        end

        if scmp('differenceAverage', featureNames) ...
        || scmp('differenceVariance', featureNames) ...
        || scmp('differenceEntropy', featureNames)
            tmp2 = [abs((I - J))' currentGLCM(sub)'];
            idx2 = 0:nGrayLevels - 1;
            for i = idx2
                pXminusY(i + 1, k) = sum(tmp2(tmp2(:, 1) == i, 2));
            end
        end

        if scmp('informationMeasureOfCorrelation1', featureNames) ...
        || scmp('informationMeasureOfCorrelation2', featureNames) ...
        || scmp('maximalCorrelationCoefficient', featureNames)
            pX(:, k) = sum(currentGLCM, 2);
            pY(:, k) = sum(currentGLCM, 1)';
        end
        if scmp('informationMeasureOfCorrelation1', featureNames)
            HXY1(k) = -nansum(currentGLCM(sub)' .* log(pX(I, k) .* pY(J, k)));
            HX(k) = -nansum(pX(:, k) .* log(pX(:, k)));
            HY(k) = -nansum(pY(:, k) .* log(pY(:, k)));
        end
        if scmp('informationMeasureOfCorrelation2', featureNames)
            HXY2(k) = -nansum(pX(I, k) .* pY(J, k) .* log(pX(I, k) .* pY(J, k)));
        end

        if scmp('energy', featureNames)
            out.energy(k) = sum(currentGLCM(sub).^2);
        end
        if scmp('contrast', featureNames)
            out.contrast(k) = sum(abs(I - J).^2 .* currentGLCM(sub));
        end

        if scmp('autoCorrelation', featureNames) ...
        || scmp('correlation', featureNames)
            autoCorrelation = sum(I .* J .* currentGLCM(sub));
            if scmp('autoCorrelation', featureNames)
                out.autoCorrelation(k) = autoCorrelation;
            end
        end

        if scmp('correlation', featureNames)
            out.correlation(k) = (autoCorrelation - uX(k) .* uY(k)) ./ (sqrt(sX(k) .* sY(k)));
        end
        if scmp('sumOfSquaresVariance', featureNames)  % N.B! Wrong implementation previously!!
            out.sumOfSquaresVariance(k) = sum(currentGLCM(sub) .* ((I - uX(k)).^2));
        end
        if scmp('homogeneity', featureNames)
            out.homogeneity(k) = sum(currentGLCM(sub) ./ (1 + (I - J).^2));
        end
        if scmp('sumAverage', featureNames) ...
        || scmp('sumVariance', featureNames)
            sumAverage = sum(bsxfun(@times, idx1', pXplusY(idx1 - 1, k)));
            if scmp('sumAverage', featureNames)
                out.sumAverage(k) = sumAverage;
            end
        end
        if scmp('sumVariance', featureNames)  % N.B! Wrong implementation previously AND in [1]
            out.sumVariance(k) = sum((idx1 - sumAverage)'.^2 .* pXplusY(idx1 - 1, k));
        end
        if scmp('sumEntropy', featureNames)
            out.sumEntropy(k) = -nansum(pXplusY(idx1 - 1, k) .* log(pXplusY(idx1 - 1, k)));
        end
        if scmp('entropy', featureNames) ...
        || scmp('informationMeasureOfCorrelation1', featureNames) ...
        || scmp('informationMeasureOfCorrelation2', featureNames)
            entropy = -nansum(currentGLCM(sub) .* log(currentGLCM(sub)));
            if scmp('entropy', featureNames)
                out.entropy(k) = entropy;
            end
        end

        if scmp('differenceAverage', featureNames) ...
        || scmp('differenceVariance', featureNames)
            differenceAverage = sum(bsxfun(@times, idx2', pXminusY(idx2 + 1, k)));
            if scmp('differenceAverage', featureNames)
                out.differenceAverage(k) = differenceAverage;
            end
        end

        if scmp('differenceVariance', featureNames)  % N.B! Wrong implementation previously!! Dissimilarity is "difference Average"
            out.differenceVariance(k) = sum((idx2 - differenceAverage).^2' .* pXminusY(idx2 + 1, k));
        end
        if scmp('differenceEntropy', featureNames)
            out.differenceEntropy(k) = -nansum(pXminusY(idx2 + 1, k) .* log(pXminusY(idx2 + 1, k)));
        end
        if scmp('informationMeasureOfCorrelation1', featureNames)
            infoMeasure1 = (entropy - HXY1(k)) ./ (max(HX(k), HY(k)));
            out.informationMeasureOfCorrelation1(k) = infoMeasure1;
        end
        if scmp('informationMeasureOfCorrelation2', featureNames)
            infoMeasure2 = sqrt(1 - exp(-2 * (HXY2(k) - entropy)));
            out.informationMeasureOfCorrelation2(k) = infoMeasure2;
        end
        if scmp('maximalCorrelationCoefficient', featureNames)
            % Correct by eps if the matrix has columns or rows that sums to zero.
            P = currentGLCM;
            if any(pX(:, k) == 0.0) || any(pY(:, k) == 0.0)
                P = P + eps;
                P = P / sum(P(:));
            end

            Q = zeros(size(P));

            for i = 1:nGrayLevels
                for j = 1:nGrayLevels
                    for l = 1:nGrayLevels
                        if pX(i, k) * pY(l, k) >= eps
                            Q(i, j) = Q(i, j) + (P(i, l) * P(j, l)) / (pX(i, k) * pY(l, k));
                        else
                            Q(i, j) = Q(i, j) + (P(i, l) * P(j, l));  % / eps;
                        end
%                         if Q(i, j) == Inf
%                             sqwcq = 1;
%                         end
                    end
                end
            end

%             % Remove some of the variance by smoothing:
%             Q = imgaussfilt(Q, [1, 1.5]);
%             sm = sum(Q, 2);
%             Q = Q ./ repmat(sm, 1, size(Q, 2));

            try
                E = eigs(Q, 2);
            catch
                try
                    E = eig(Q);
                catch
                    fprintf('Could not compute the maximalCorrelationCoefficient!\n');
                end
            end

            if isreal(E(2))
                e2 = E(2);
            else
                e2 = min(real(E(1)), real(E(2)));
            end

            out.maximalCorrelationCoefficient(k) = e2;
        end

        % Haralick-like features:
        % ----------------------
        if scmp('dissimilarity', featureNames)
            dissimilarity = sum(abs(I - J) .* currentGLCM(sub));
            out.dissimilarity(k) = dissimilarity;
        end
        if scmp('clusterShade', featureNames)
            out.clusterShade(k) = sum((I + J - uX(k) - uY(k)).^3 .* currentGLCM(sub));
        end
        if scmp('clusterProminence', featureNames)
            out.clusterProminence(k) = sum((I + J - uX(k) - uY(k)).^4 .* currentGLCM(sub));
        end
        if scmp('maximumProbability', featureNames)
            out.maximumProbability(k) = max(currentGLCM(:));
        end
        if scmp('inverseDifference', featureNames)
            out.inverseDifference(k) = sum(currentGLCM(sub) ./ (1 + abs(I - J)));
        end
    end
end

% GLCM Features (Soh, 1999; Haralick, 1973; Clausi 2002)
%  f1. Angular Second Moment / Energy / Uniformity
%  f2. Contrast / Inertia
%  f3. Correlation
%  f4. Sum of Squares: Variance
%  f5. Inverse Difference Moment / Homogeneity
%  f6. Sum Average
%  f7. Sum Variance
%  f8. Sum Entropy
%  f9. Entropy
% f10. Difference Variance
% f11. Difference Entropy
% f12. Information Measure of Correlation 1
% f13. Information Measure of Correlation 2
% f14. Maximal Correlation Coefficient
% f15. Autocorrelation
% f16. Dissimilarity
% f17. Cluster Shade
% f18. Cluster Prominence
% f19. Maximum Probability
% f20. Inverse Difference
% f21. Difference Average

function contains = scmp(string, list)

    contains = any(strcmpi(list, string)) || any(strcmpi(list, 'all'));

end
