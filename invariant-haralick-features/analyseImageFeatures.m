function [mse, bias2, variance, mse_std, idealGLCMs, idealFeatures] ...
    = analyseImageFeatures(images, GLCMSizes, nPoints, neighbourhood, ...
                           bounds, nPointsIdeal, GLCMReps, featureNames, ...
                           DBID, DBPath, storeSE)
%analyseImageFeatures Analyse the statistics of invariant GLCM-based image features.
% 
%   Textförklaring
%
%   Parameters
%   ----------
%   images : cell array of images
%       A cell array of images to be analysed. The images represent the
%       same texture.
%
%   GLCMSizes : Vector of natural numbers.
%       A vector that contains the GLCM sizes for which the image features
%       should be analysed.
%
%   nPoints : vector of int
%       A vector with the number of points to use when constructing the
%       GLCMs.
%
%   neighbourhood : 3^d logical array
%       A 3-by-3-by-... (d times) logical array with 1s for neighbours to
%       analyse, and 0s for neighbours to ignore.
%
%   bounds : 1-by-4 vector of float
%       The min and max values for the grey-value pairs used when
%       generating the GLCM. The format is [minx, maxx, miny, maxy], where
%       x is the first dimension and y is the second dimension. If bounds
%       is an empty vector ([]), the minimum and maximum values will be
%       derived from the point cloud.
%
%   nPointsIdeal : int
%       The number of points to use when computing the "ideal" GLCM, that
%       is used as reference.
%
%   GLCMReps : int
%       The number of times to sample GLCMs for each GLCM size. This number
%       will decide the quality of the output statistics.
%
%   featureNames : cell array of strings
%       A cell array with the string names of the features to compute. The
%       available names are:
%           energy, contrast, correlation, sumOfSquaresVariance,
%           homogeneity, sumAverage, sumVariance, sumEntropy, entropy,
%           differenceVariance, differenceEntropy,
%           informationMeasureOfCorrelation1,
%           informationMeasureOfCorrelation2,
%           maximalCorrelationCoefficient, autoCorrelation, dissimilarity,
%           clusterShade, clusterProminence, maximumProbability,
%           inverseDifference, differenceAverage
%
%   DBID : string, optional
%       An identifier for the image database. If specified the intermediate
%       results will be saved to file, identified by the DBID, in a folder
%       specified by DBPath.
%
%   DBPath : string, optional
%       If DBID is given, this is the path to the folder where the files
%       are saved. If not specified, files are saved in the current working
%       directory, in a subfolder "data".
%
%   Returns
%   -------
%   mse : cell array of structs with vectors of MSE for each feature
%       The approximated mean squared error of the feature value
%       estimation. The cell array has size equal to the number of elements
%       in nPoints. Each struct has members for each feature in
%       featureNames and they are vectors of length equal to the length of
%       GLCMSizes.
%
%   bias2 : cell array of structs with vectors of squared bias for each
%       feature. The approximated squared bias of the feature value
%       estimation. The cell array has size equal to the number of elements
%       in nPoints. Each struct has members for each feature in
%       featureNames and they are vectors of length equal to the length of
%       GLCMSizes.
%
%   variance : cell array of structs with vectors of variance for each
%       feature. The approximated variance of the feature value estimation.
%       The cell array has size equal to the number of elements in nPoints.
%       Each struct has members for each feature in featureNames and they
%       are vectors of length equal to the length of GLCMSizes.
%
%   mse_std : cell array of structs with vectors of standard deviation of
%       the MSE for each feature. The approximated standard deviation of
%       the MSE of the feature value estimation. The cell array has size
%       equal to the number of elements in nPoints. Each struct has members
%       for each feature in featureNames and they are vectors of length
%       equal to the length of GLCMSizes.
%
%   idealGLCMs : cell array of GLCMs
%       The GLCMs are calculated with nPointsIdeal points and the length of
%       the cell array is equal to the length of GLCMSizes.
%
%   idealFeatures : cell array of structs with the values for each feature
%       The feature values of the "ideal" GLCMs. The cell array has size
%       equal to the number of elements in nPoints. Each struct has members
%       for each feature in featureNames and they contain the feature
%       values.

%   See also .

%   Copyright 2016.

has_dbid = false;
if nargin >= 9
    has_dbid = true;
end
db_path = [pwd, '/data'];
if nargin >= 10
    db_path = DBPath;
end
if nargin < 11
    storeSE = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%
% Generate point cloud %
%%%%%%%%%%%%%%%%%%%%%%%%
if has_dbid
    filename_ptc = [db_path, '/', DBID, '_point_cloud.mat'];
end
if has_dbid && (exist(filename_ptc, 'file') == 2)
    DATA = load(filename_ptc);
    pts = DATA.pts;
else
    pts = multiplePointCloud(images, neighbourhood);
    if has_dbid
        save(filename_ptc, 'pts');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate asymptotic GLCMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(bounds)
    minx = min(pts(:, 1));
    maxx = max(pts(:, 1));
    miny = min(pts(:, 2));
    maxy = max(pts(:, 2));

    bounds = [minx, maxx, miny, maxy];
end
if has_dbid
    filename_ideal_GLCMs = [db_path, '/', DBID, '_ideal_GLCMs.mat'];
end
if has_dbid && (exist(filename_ideal_GLCMs, 'file') == 2)
    DATA = load(filename_ideal_GLCMs);
    idealGLCMs = DATA.idealGLCMs;
else
    idealGLCMs = cell(1, length(GLCMSizes));
    prog = Progressor('Computing ideal GLCMs');
    for i = 1:length(GLCMSizes)
        GT = pt2glcm(pts, GLCMSizes(i), bounds(1), bounds(2), bounds(3), bounds(4));
        idealGLCMs{i} = GT;
        prog.setProgress(i / length(GLCMSizes));
    end
    prog.setProgress(1);
    clear('prog');
    if has_dbid
        save(filename_ideal_GLCMs, 'idealGLCMs');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate data for MSE curves %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if has_dbid
    filename_mse = [db_path, '/', DBID, '_MSE.mat'];
end
if has_dbid && (exist(filename_mse, 'file') == 2)
    DATA = load(filename_mse);
    mse = DATA.mse;
    bias2 = DATA.bias2;
    variance = DATA.variance;
    mse_std = DATA.mse_std;
    idealFeatures = DATA.idealFeatures;
else
    idealFeatures = generateFeatures(idealGLCMs, featureNames);
    mse = cell(1, length(nPoints));
    bias2 = cell(1, length(nPoints));
    variance = cell(1, length(nPoints));
    mse_std = cell(1, length(nPoints));
    se = cell(1, length(nPoints));
    prog = Progressor('Generating statistics');
    for i = 1:length(nPoints)
        [mse{i}, bias2{i}, variance{i}, mse_std{i}, se{i}] ...
            = computeMSE(pts, GLCMSizes, nPoints(i), GLCMReps, ...
                         idealFeatures(end), neighbourhood, bounds, ...
                         featureNames);
         prog.setProgress(i / length(nPoints));
    end
    prog.setProgress(1);
    clear('prog');
    if has_dbid
        if ~storeSE
            se = NaN;
        end
        save(filename_mse, 'mse', 'bias2', 'variance', 'mse_std', 'se', 'idealFeatures');
    end
end
