function plotFCurves(allMSE, pairwiseMeanDistance, featureNames, nPoints)

    [~, C1, C2] = getFeatureNames();

    Ncolors = 256;
%     cmap = [linspace(1, 0, Ncolors / 2)', 0 * ones(Ncolors / 2, 1), linspace(0, 1, Ncolors / 2)'
%             0 * ones(Ncolors / 2, 1), linspace(0, 1, Ncolors / 2)', linspace(1, 0, Ncolors / 2)'];
    cmap = [linspace(1, 0.9, Ncolors)', linspace(0, 0.9, Ncolors)', linspace(0, 0.9, Ncolors)'];

    pixels = linspace(1, Ncolors, length(nPoints));

    figureHandle = figure();
    figureHandle.Position = [10, 10, 1200, 750];
    delta = [0.07, 0.09];
    posMap = [1, 4, 5, 6, 7, 8, 9];
    posidx = 1;
    for i = 1:length(featureNames)
        featureName = featureNames{i};

        if any(strcmp(C1, featureName))
            modsubplot(3, 3, posMap(posidx), delta);
            mse = allMSE.(featureName);
            dist = pairwiseMeanDistance.(featureName);
            Z  = sqrt(dist) ./ (2 * sqrt(mse));
            P = 1 - 2 * normcdf(-Z);

            I = imresize(P, [Ncolors, Ncolors]);
%             I = imfilter(I, fspecial('gaussian', 3, 0.5));
%             maxi = max(maxi, max(I(:)));
%             mini = min(mini, min(I(:)));
%             maxi = log10(23.64);
%             mini = log10(2.96e-5);
            mini = 0;
            maxi = 1;
            h = imagesc(I, [mini, maxi]);
            set(gca, 'Ydir', 'Normal');
            set(gca, 'TickLabelInterpreter', 'LaTeX');

%             ticks = get(gca, 'XTick');
%             ticks = [50, 150, 250];
            ticks = [1000, 10000, 100000];
            newTicks = round(interp1(nPoints, pixels, ticks));
            tickLabelsX = cell(1, length(ticks));
            for j = 1:length(ticks)
                e = floor(log10(ticks(j)));
%                 e = floor(log10(newTicks(j)));
%                 d = round(newTicks(j) / (10^e));
                tickLabelsX{j} = sprintf('$10^{%d}$', e);
            end
            set(gca, 'XTick', newTicks);
            set(gca, 'XTickLabel', tickLabelsX);

            set(gca, 'YTick', [50, 150, 250]);
            set(gca, 'YTickLabel', {'50', '150', '250'});

            set(gca, 'TickLabelInterpreter', 'LaTeX');
            h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
            h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);

            if posMap(posidx) == 1
                ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posMap(posidx) == 4
                ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posMap(posidx) == 7
                xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posMap(posidx) == 8
                xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posMap(posidx) == 9
                xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end

%             set(gca, 'Ydir', 'Normal');
            hold('on');
%             contour(I, 'k-', 'ShowText', 'on');
            title(featureNameMap(featureName, true), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
            posidx = posidx + 1;
%             colormap(hot(Ncolors));
            colormap(cmap);
        end
    end
    h = colorbar('southoutside');
%     h.Position(1) = 0.367;
    h.Position(1) = 0.38;
    h.Position(2) = 0.79;
%     h.Position(3) = h.Position(3) * 2.185;
    h.Position(3) = h.Position(3) * 2.29;
    h.Position(4) = h.Position(4) * 3.0;
    h.TickLabelInterpreter = 'LaTeX';
    h.FontSize = fontSize('ticklabel', figureHandle);



    figureHandle = figure();
    figureHandle.Position = [10, 10, 1200, 1000];
    delta = [0.07, 0.07];
    posidx = 1;
    for i = 1:length(featureNames)
        featureName = featureNames{i};

        if any(strcmp(C2, featureName))
            modsubplot(4, 3, posidx, delta);
            mse = allMSE.(featureName);
            dist = pairwiseMeanDistance.(featureName);

            Z  = sqrt(dist) ./ (2 * sqrt(mse));
            P = 1 - 2 * normcdf(-Z);

            I = imresize(P, [Ncolors, Ncolors]);
%             I = imfilter(I, fspecial('gaussian', 3, 0.5));
%             maxi = max(maxi, max(I(:)));
%             mini = min(mini, min(I(:)));
%             maxi = log10(23.64);
%             mini = log10(2.96e-5);
            mini = 0;
            maxi = 1;
            h = imagesc(I, [mini, maxi]);
            set(gca, 'Ydir', 'Normal');
            set(gca, 'TickLabelInterpreter', 'LaTeX');

%             ticks = get(gca, 'XTick');
%             newTicks = round(interp1(pixels, nPoints, ticks));
%             tickLabelsX = cell(1, length(ticks));
%             for j = 1:length(ticks)
%                 e = floor(log10(newTicks(j)));
%                 d = round(newTicks(j) / (10^e));
%                 tickLabelsX{j} = sprintf('$%d \\cdot 10^{%d}$', d, e);
%             end
%             set(gca, 'XTickLabel', tickLabelsX);

%             ticks = get(gca, 'XTick');
            ticks = [1000, 10000, 100000];
            newTicks = round(interp1(nPoints, pixels, ticks));
            tickLabelsX = cell(1, length(ticks));
            for j = 1:length(ticks)
                e = floor(log10(ticks(j)));
                tickLabelsX{j} = sprintf('$10^{%d}$', e);
            end
            set(gca, 'XTick', newTicks);
            set(gca, 'XTickLabel', tickLabelsX);

            set(gca, 'YTick', [50, 150, 250]);
            set(gca, 'YTickLabel', {'50', '150', '250'});

            hold('on');

            set(gca, 'TickLabelInterpreter', 'LaTeX');
            h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
            h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);

            if posidx == 1 || posidx == 4 || posidx == 7
                ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posidx == 10
                xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end
            if posidx == 11 || posidx == 12
                xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
                set(gca, 'TickLabelInterpreter', 'LaTeX');
            end

%             contour(I, 'k-', 'ShowText', 'on');
            title(featureNameMap(featureName), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
            posidx = posidx + 1;
%             colormap(hot(Ncolors));
            colormap(cmap);
        end
    end
    h = colorbar();
    h.Position(1) = 0.9575;
    h.Position(2) = 0.15;
    h.Position(3) = h.Position(3) * 0.9;
    h.Position(4) = h.Position(4) * 4.3;
    h.TickLabelInterpreter = 'LaTeX';
    h.FontSize = fontSize('ticklabel', figureHandle);
