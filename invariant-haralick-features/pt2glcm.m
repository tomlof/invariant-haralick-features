function G = pt2glcm(X, n, minx, maxx, miny, maxy)

    X = double(X);

    X(:, 1) = X(:, 1) - repmat(minx, size(X, 1), 1);
    X(:, 1) = X(:, 1) ./ (repmat(maxx, size(X, 1), 1) - repmat(minx, size(X, 1), 1));

    X(:, 2) = X(:, 2) - repmat(miny, size(X, 1), 1);
    X(:, 2) = X(:, 2) ./ (repmat(maxy, size(X, 1), 1) - repmat(miny, size(X, 1), 1));

    X = floor(n * X);
    X(X == n) = n - 1;  % Happens when the value is exactly 1.
    X = X + 1;  % We want indices 1-64, not 0-63.

    G = zeros(n, n);
    for i = 1:size(X, 1)
        G(X(i, 1), X(i, 2)) = G(X(i, 1), X(i, 2)) + 1;
    end

    G = G / sum(G(:));
