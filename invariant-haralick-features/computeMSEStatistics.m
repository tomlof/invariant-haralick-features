function [mse, variance, bias2, mse_var] = computeMSEStatistics(Xn, X0, B)

    mse = [];
    variance = [];
    bias2 = [];

    n = length(Xn);
    for i = 1:B
%         ids = setdiff(1:n, i);
        ids = randi(n, n, 1);
        X_ = Xn(ids);

        [mse(i), variance(i), bias2(i)] = computeStatistic(X_, X0);
    end

    mse_var = var(mse);
    mse = mean(mse);
    variance = mean(variance);
    bias2 = mean(bias2);

end

function [mse, variance, bias2] = computeStatistic(Xn, X0)

    bias2 = (mean(Xn) - X0)^2;
    variance = var(Xn);
    mse = bias2 + variance;

end
