function images = readImages(library, class)

    path = dataPath();

    if strcmpi(library, 'kylberg')
        path = [path, class, '-without-rotations\'];

        files = dir(path);
        images = {};
        for i = 1:length(files)
            file = files(i);
            if ~file.isdir
                I = double(imread([path, file.name]));
                I = imresize(I, size(I) / 2, 'bilinear');
                images = {images{:}, I};
            end
        end
    else
        throw(MException('readImages:UnknownLibrary', ...
                         'Unknown library name!'));
    end
