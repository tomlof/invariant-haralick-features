function h = modsubplot(m,n,p,delta)
 % Like subplot but with a extra vector delta 1 x 2 vector of distances
 % between plots. OBS normalized units. Total length of the figure is 1.
 
 figX = (1-delta(1)*(n+1))/n;
 figY = (1-delta(2)*(m+1))/m;
 
 [N,M] = ind2sub([n,m],p);
 M = m-M+1;
 
 X = figX*(N-1)+delta(1)*N;
 Y = figY*(M-1)+delta(2)*M;
 h = axes('Position',[X,Y,figX,figY],'units','Normalized');
end

