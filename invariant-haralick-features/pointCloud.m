function points = pointCloud(x, neighborhood, randomFraction)
% Extract a GLCM-like point-cloud from a image.
%
% Input: 
% x                 - ND image all dimensions must be atleast 3 big.
% neighboorhood     - Describes what neighboor to include [3^N] for a ND image. 
%                     Should be logical. 
%
% points            - 2 x n vector of points
% randomFraction    - The random fraction of the points to use. Optional,
%                     default == 1.
%
% Example: 
% X = randn(25,25,10);
% N = false(3,3,3);
% N([1,3],2,2) = true;
% N(2,[1,3],2) = true;
% N(2,2,[1,3]) = true;
% fraction = 0.1;
% points = pointCloud(X,N,fraction);
% plot(points(1,:),points(2,:),'.');

nDim = numel(size(x));

% Check neighboorhood
if (sum(size(neighborhood) == 3) ~= nDim)
    error('pointCloud:input', 'neighborhood is of wrong size.'); 
end

if (~islogical(neighborhood))
    error('pointCloud:input', 'neighborhood is of wrong type.');  
end

points = [];

dimArray = cell(1, nDim);
[dimArray{:}] = deal([1, 2, 3]);

lc = LoopCounter(dimArray);

% Loop over all selected neighbors
while (lc.running)
    di = lc.value;

    % Get cloud for one neighboor
    if (neighborhood(di{:}))
        points = [ points, getPoints(x, nDim, di) ];      %#ok<AGROW>
    end

    lc = lc.increment();
end

% Select random points
if (nargin > 2)
    nPoints = size(points, 2);
    points = points(:, rand(1, nPoints) <= randomFraction);
end

points = points';


% Helper functions
function P = getPoints(X, nDim, di)
    stop = size(X);

    select1 = cell(size(stop));
    select2 = cell(size(stop));

    for ii = 1:nDim
        switch (di{ii})
            case 1
                select1{ii} = 2:stop(ii);
                select2{ii} = 1:(stop(ii)-1);
            case 2
                select1{ii} = 1:stop(ii);
                select2{ii} = 1:stop(ii);
            case 3
                select2{ii} = 2:stop(ii);
                select1{ii} = 1:(stop(ii)-1);
        end
    end

    p1 = X(select1{:});
    p2 = X(select2{:});
    P = [p1(:).';p2(:).'];
