function plotOptima(GLCMSizes, nPoints, mse, mse_std, se, GLCMReps)

    colors = [0.0000, 0.4470, 0.7410
              0.8500, 0.3250, 0.0980
              0.9290, 0.6940, 0.1250
              0.4940, 0.1840, 0.5560
              0.4660, 0.6740, 0.1880
              0.3010, 0.7450, 0.9330
              0.6350, 0.0780, 0.1840];

    delta1 = [0.06, 0.12];
    delta = [0.06, 0.06];

    %%%%%%%%%%%%
    % MSE plot %
    %%%%%%%%%%%%
    figureHandle = figure(1);
    figureHandle.Position = [10, 10, 1200, 400];

    figureHandle = figure(2);
    figureHandle.Position = [10, 10, 1200, 800];

    figureHandle = figure(1);
    modsubplot(1, 2, 1, delta1);
    j = 10;
    N = nPoints(j);
    err = mse{j}.energy;
    semilogy(GLCMSizes, err, 'k-', 'LineWidth', 1);
    hold('on');
    [minerr, idx] = min(err);
    h = plot(GLCMSizes(idx), minerr, 'ro');
    h.LineWidth = 2;
    h.MarkerSize = 10;
    text(GLCMSizes(idx) + 7, minerr, sprintf('$n = %.0f$', GLCMSizes(idx)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
    text(GLCMSizes(end) + 1, err(end), sprintf('$N = %d$', N), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
    ylim([0.019, 1000]);
    xlim([0, 320]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    title(featureNameMap('Energy'), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    ylabel('Mean squared error', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    text(15, 3.2e2, 'a', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));

    figureHandle = figure(2);
    modsubplot(2, 2, 1, delta);
    energy_minima = [];
    for j = 1:length(nPoints)
        N = nPoints(j);
        err = mse{j}.energy;

        if mod(j, 2) == 1
            semilogy(GLCMSizes, err, 'k-');
            hold('on');
        end

        [minerr, idx] = min(err);
        energy_minima = [energy_minima, GLCMSizes(idx)];
        if mod(j, 2) == 1
            h = plot(GLCMSizes(idx), minerr, 'ro');
            h.LineWidth = 2;
            h.MarkerSize = 10;

            text(GLCMSizes(end) + 1, err(end), sprintf('$N = %d$', N), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
    end
    ylim([5.2e-4, 0.40e6]);
    xlim([0, 335]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    ylabel('Mean squared error', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    title(featureNameMap('Energy'), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    text(16, 5e4, 'a', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));

    figureHandle = figure(2);
    modsubplot(2, 2, 3, delta);
    h = semilogx(nPoints, energy_minima, 'ro');
    hold('on');
    X = nPoints';
    X = [ones(size(X, 1), 1), X, log(X)];
    beta = pinv(X) * energy_minima';
    newX = linspace(nPoints(1), nPoints(end), 100)';
    newX = [ones(size(newX, 1), 1), newX, log(newX)];
    predminima = newX * beta;
    h.LineWidth = 2;
    h.MarkerSize = 10;
    plot(newX(:, 2), predminima, '--k');
    ylim([9, 67]);
    xlim([1.6e2, 1.15e5]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    text(2.2e2, 61, 'c', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));
%     text(2.2e2, 25, 'c', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));




    figureHandle = figure(1);
    modsubplot(1, 2, 2, delta1);
    z = 1.96;
    j = 10;
    N = nPoints(j);
    err = mse{j}.homogeneity;
    if iscell(se)
        sqerr = se{j}.homogeneity;
        for i = 1:10:GLCMReps
            semilogy(GLCMSizes, max(1.04e-8, sqerr(:, i)), ':', 'LineWidth', 1, ...
                     'Color', [0.9, 0.9, 0.9]);
            hold('on');
        end
    end
    se = sqrt(GLCMReps) * mse_std{j}.homogeneity;  % Standardavvikelsen i SE
    bound = err(end) + z * se(end);  % Slutv�rdet plus 2 standardavvikelser
    idx = find(err < bound, 1);  % F�rsta mindre �n gr�nsen
    idx = max(1, min(idx, length(GLCMSizes)));  % Sanity check
    opt = err(idx);  % Opimala v�rdet f�r GLCM-storleken
    semilogy(GLCMSizes, err, 'k-', 'LineWidth', 1);
    hold('on');
    h = plot(GLCMSizes(idx), opt, 'ro');
    h.LineWidth = 2;
    h.MarkerSize = 10;
    plot([GLCMSizes(1), GLCMSizes(end)], [bound, bound], 'r:', 'LineWidth', 2);
    plot([GLCMSizes(end), GLCMSizes(end)], [err(end), bound], 'r:', 'LineWidth', 2);
    text(GLCMSizes(idx) + 5, 1.5 * opt, sprintf('$n = %.0f$', GLCMSizes(idx)), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
    text(GLCMSizes(end) + 1, err(end), sprintf('$N = %d$', N), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
    ylim([1e-8, 1e-4]);
    xlim([0, 320]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    title(featureNameMap('Homogeneity'), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
%     ylabel('Mean squared error', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    text(291, 3.75e-5, 'b', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));


    figureHandle = figure(2);
    modsubplot(2, 2, 2, delta);
    homogeneity_minima = [];
    z = 1.96;
    for j = 1:length(nPoints)
        N = nPoints(j);
        err = mse{j}.homogeneity;
        se = sqrt(GLCMReps) * mse_std{j}.homogeneity;  % Standardavvikelsen i SE
        bound = err(end) + z * se(end);  % Slutv�rdet plus 2 standardavvikelser
        idx = find(err < bound, 1);  % F�rsta mindre �n gr�nsen
        idx = max(1, min(idx, length(GLCMSizes)));  % Sanity check
        opt = err(idx);  % Opimala v�rdet f�r GLCM-storleken
        homogeneity_minima = [homogeneity_minima, GLCMSizes(idx)];

        if mod(j, 2) == 1
            semilogy(GLCMSizes, err, 'k-');
            hold('on');

            h = plot(GLCMSizes(idx), opt, 'ro');
            h.LineWidth = 2;
            h.MarkerSize = 10;

            text(GLCMSizes(end) + 1, mean(err(end-10:end)), sprintf('$N = %d$', N), 'Interpreter', 'LaTeX', 'FontSize', fontSize('text', figureHandle));
        end
    end
    ylim([1.25e-9, 1e-4]);
    xlim([0, 335]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
%     ylabel('Mean squared error', 'Interpreter', 'LaTeX');
    xlabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    title(featureNameMap('Homogeneity'), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    text(306, 3.25e-5, 'b', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));


    figureHandle = figure(2);
    modsubplot(2, 2, 4, delta);
    h = semilogx(nPoints, homogeneity_minima, 'ro');
    hold('on');
    X = nPoints';
    X = [ones(size(X, 1), 1), X, log(X)];
    beta = pinv(X) * homogeneity_minima';
    newX = linspace(nPoints(1), nPoints(end), 100)';
    newX = [ones(size(newX, 1), 1), newX, log(newX)];
    predminima = newX * beta;
    h.LineWidth = 2;
    h.MarkerSize = 10;
    plot(newX(:, 2), predminima, '--k');
    ylim([9, 49]);
    xlim([1.7e2, 1.2e5]);
    set(gca, 'TickLabelInterpreter', 'LaTeX');
    h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
    xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
%     ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
    text(6.95e4, 13, 'd', 'Interpreter', 'LaTeX', 'FontSize', fontSize('number', figureHandle));
