function [figureWidth, figureHeight, axesPositions] = axesGrid(horizontalSize, verticalSize, axesIndices)

figureWidth = sum(horizontalSize);
figureHeight = sum(verticalSize);

numberOfAxes = size(axesIndices,1);

axesPositions = zeros(numberOfAxes,4);

horizontalEdges = [0 cumsum(horizontalSize)];
verticalEdges = [0 cumsum(verticalSize)];

for i = 1:numberOfAxes
    axesPositions(i,1) = horizontalEdges(axesIndices(i,1))/figureWidth;
    axesPositions(i,2) = verticalEdges(axesIndices(i,3))/figureHeight;
    axesPositions(i,3) = (horizontalEdges(axesIndices(i,2)+1) - horizontalEdges(axesIndices(i,1)))/figureWidth;
    axesPositions(i,4) = (verticalEdges(axesIndices(i,4)+1) - verticalEdges(axesIndices(i,3)))/figureHeight;
end
