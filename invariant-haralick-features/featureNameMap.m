function mapped_name = featureNameMap(name, short)

    if nargin < 2
        short = false;
    end

   if iscell(name)
       mapped_name = cell(size(name));
       for i = 1:length(name)
           mapped_name{i} = map(name{i}, short);
       end
   else
       mapped_name = map(name, short);
   end
end

function mapped_name = map(name, short)

    if strcmpi(name, 'energy')
        mapped_name = 'Angular Second Moment';
    elseif strcmpi(name, 'contrast')
        mapped_name = 'Contrast';
    elseif strcmpi(name, 'correlation')
        mapped_name = 'Correlation';
    elseif strcmpi(name, 'sumOfSquaresVariance')
        mapped_name = 'Sum of Squares: Variance';
    elseif strcmpi(name, 'homogeneity')
        mapped_name = 'Inverse Difference Moment';
    elseif strcmpi(name, 'sumAverage')
        mapped_name = 'Sum Average';
    elseif strcmpi(name, 'sumVariance')
        mapped_name = 'Sum Variance';
    elseif strcmpi(name, 'sumEntropy')
        mapped_name = 'Sum Entropy';
    elseif strcmpi(name, 'entropy')
        mapped_name = 'Entropy';
    elseif strcmpi(name, 'differenceVariance')
        mapped_name = 'Difference Variance';
    elseif strcmpi(name, 'differenceEntropy')
        mapped_name = 'Difference Entropy';
    elseif strcmpi(name, 'differenceEntropy')
        mapped_name = 'Difference Entropy';
    elseif strcmpi(name, 'informationMeasureOfCorrelation1')
        if ~short
            mapped_name = 'Information Measure of Correlation 1';
        else
            mapped_name = 'Info. Measure of Corr. 1';
        end
    elseif strcmpi(name, 'informationMeasureOfCorrelation2')
        if ~short
            mapped_name = 'Information Measure of Correlation 2';
        else
            mapped_name = 'Info. Measure of Corr. 2';
        end
    elseif strcmpi(name, 'maximalCorrelationCoefficient')
        mapped_name = 'Maximal Correlation Coefficient';

    elseif strcmpi(name, 'autoCorrelation')
        mapped_name = 'Autocorrelation';
    elseif strcmpi(name, 'dissimilarity')
        mapped_name = 'Dissimilarity';
    elseif strcmpi(name, 'clusterShade')
        mapped_name = 'Cluster Shade';
    elseif strcmpi(name, 'clusterProminence')
        mapped_name = 'Cluster Prominence';
    elseif strcmpi(name, 'maximumProbability')
        mapped_name = 'Maximum Probability';
    elseif strcmpi(name, 'inverseDifference')
        mapped_name = 'Inverse Difference';
    elseif strcmpi(name, 'differenceAverage')
        mapped_name = 'Difference Average';
    else
        mapped_name = name;
    end
end