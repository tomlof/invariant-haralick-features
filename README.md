An implementation of the asymptotically invariant modified Haralick texture features proposed in "Bit Depth-Invariant GLCM-Based Texture Features" by Löfstedt, Brynolfsson, Nyholm and Garpebring.

### How to use it? ###

* Download or clone the repository.
* Download the data (see details below).
* Edit `dataPath` (see details below).
* Run `main.m` (tested in in Matlab 2015 and 2016).
* Edit `main.m` to change what is computed and what plots to show. The first few lines define what is generated/plotted.

### Dependencies ###

Some of the plots use the `jbfill` function by John Bockstege ([you'll find it here](https://se.mathworks.com/matlabcentral/fileexchange/13188-shade-area-between-two-curves/)) to plot the area between curves. The `jbfill` function is included in this repository. See the attached license file for the `jbfill` function.

### Data ###
The scripts require the [Kylberg Texture Dataset v. 1.0](http://www.cb.uu.se/~gustaf/texture/). To run them you need to download the data (without rotated patches) from [here](http://www.cb.uu.se/~gustaf/texture/data/without-rotations-zip/).

When you have downloaded and unpacked it, you need to update the path to the data in the script file `dataPath`.
